package restart.event.accelerator.lambda;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import com.amazonaws.greengrass.javasdk.IotDataClient;
import com.amazonaws.greengrass.javasdk.model.PublishRequest;

public class MachineEventHandlerTask extends TimerTask {

	public void run() {

		System.out.println(">>>>>>>>> EventStartStop.handleRequest : START at : " + new Date());

		IotDataClient iotDataClient = new IotDataClient();

		System.out.println(">>>>>>>>>>>>> EventStartStop.handleRequest : Read Machine Data and Convert to JSON format");

		List<String> ess = FrameJSONPacketFromMachineData.getOutputStreamFromSocketForEventStart();

		for (String start : ess) {
			publishEventStartMessage(start, iotDataClient, "emi/machine/event");
		}

		System.out.println(">>>>>>>>>>>> EventStartStop.handleRequest : COMPLETE");
	}

	/**
	 * Publish Message to IoT client
	 * 
	 * @param str
	 * @param iotDataClient
	 */
	private void publishEventStartMessage(String str, IotDataClient iotDataClient, String topicName) {
		try {
			PublishRequest publishRequest = new PublishRequest().withTopic(topicName)
					.withPayload(ByteBuffer.wrap(str.getBytes()));

			iotDataClient.publish(publishRequest);

			System.out.println(">>>>>>> EventStartStop : Message Sent Successfully. MessageData: " + str);

		} catch (Exception e) {
			System.out
					.println(">>>>>> Exception occurred while processing/publishing EventStartStop to IoT Topic. Data: "
							+ str + ", ErrorMessage: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
