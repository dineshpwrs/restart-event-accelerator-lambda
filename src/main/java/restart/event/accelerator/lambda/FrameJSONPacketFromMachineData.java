package restart.event.accelerator.lambda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import com.amazonaws.greengrass.dao.DefaultEmiDowntimeFactDao;
import com.amazonaws.greengrass.dao.EmiDowntimeFactDao;
import com.amazonaws.greengrass.pojo.EventStartStop;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class FrameJSONPacketFromMachineData {

	static ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	private final static EmiDowntimeFactDao donwtimeFactDao = DefaultEmiDowntimeFactDao.instance;

	public FrameJSONPacketFromMachineData() {
		super();
	}

	/**
	 * Request data from socket connection
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List<String> getOutputStreamFromSocketForEventStart() {
		Socket socketStart = null;
		List<String> ess = new ArrayList<>();
		String host = "10.0.3.164";
		int port = 6549;
		BufferedReader brStart = null;
		PrintWriter os = null;
		try {
			// InetAddress address = InetAddress.getByName(host);
			socketStart = new Socket(host, port);

			DateFormat sdf = new SimpleDateFormat("MMddHHmm");

			String trpTime = sdf.format(org.apache.commons.lang3.time.DateUtils.addMinutes(new Date(), -30));

			DateFormat sdfTrp = new SimpleDateFormat("MMddHHmm");
			Date currentTrpDate = sdfTrp.parse(trpTime);

			System.out.println("Current TRP date: " + currentTrpDate);

			String dbLatestTrp = donwtimeFactDao.getLatestTrpTime();

			Date latestTrpDate = sdfTrp.parse(dbLatestTrp);

			System.out.println("DB Latest TRP date: " + latestTrpDate);
			System.out.println("XYZ: " + DateUtils.addMinutes(currentTrpDate, 55));

			boolean isDate = isDateInBetweenIncludingEndPoints(currentTrpDate, DateUtils.addMinutes(currentTrpDate, 50),
					latestTrpDate);

			if (isDate) {
				trpTime = dbLatestTrp;
			}

			// Send the message to the server
			os = new PrintWriter(socketStart.getOutputStream(), true);
			String sendMessage = "GetData -s " + trpTime + " EventLog";
			os.println(sendMessage);
			System.out.println("Message sent to the server : " + sendMessage);

			// Get the return message from the server
			brStart = new BufferedReader(new InputStreamReader(socketStart.getInputStream()));

			System.out.println("After BufferedReader start");

			ess = eventStartStopParse(brStart, trpTime);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> Reading machine response from : " + brStart);
		} catch (Exception e) {
			System.out
					.println(">>>>>>>>>>>>>>>>>>>>>>>>>> Exception occurred while Reading Machine Data. ErrorMessage: "
							+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {

				if (os != null) {
					os.flush();
					os.close();
				}

				if (brStart != null) {
					brStart.close();
				}

				if (socketStart != null && !socketStart.isClosed()) {
					socketStart.close();
				}

			} catch (Exception e) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> Exception occurred while closing socket. ErrorMessage: "
						+ e.getMessage());
				e.printStackTrace();
			}
		}
		return ess;
	}

	/**
	 * Frame JSON Packet from BufferedReader
	 * 
	 * @param bur
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static List<String> eventStartStopParse(BufferedReader brStart, String trpTime) throws Exception {
		List<String> eventStartStop = new ArrayList<>();
		List<String> al = null;
		EventStartStop staStp = new EventStartStop();
		String st = null;
		st = brStart.readLine();
		String responsestart = null;
		int check = 1;
		while (st != null && !st.equals("E")) {
			int flag = 0;
			if (st.startsWith(">Q1")) {
				String[] arr = st.split(";");
				staStp.setMachine(arr[1]);
				staStp.setTrpTime(trpTime);
			} else if (st.startsWith(">LL")) {
				String[] arr = st.split(";");
				staStp.setLayout(arr[1]);
			} else if (st.startsWith(">STA") && check == 1) {
				String[] arr22 = st.split(";");
				staStp.setStartTimestamp(arr22[1]);
				staStp.setStopTimestamp("null");
				responsestart = ow.writeValueAsString(staStp);
				eventStartStop.add(responsestart);
				staStp.setStopTimestamp("null");
			} else if (st.startsWith(">STP")) {
				String[] arr = st.split(";");
				staStp.setStopTimestamp(arr[1]);
				staStp.setReason(arr[2]);
				check = 2;
				if (flag == 0) {
					st = brStart.readLine();
					flag = 1;
				}

				if (st != null && !st.equals("E")
						&& (st.startsWith(">RS") || st.startsWith(">FO") || st.startsWith(">MRS"))) {
					al = new ArrayList<>();
					while (st != null && !st.equals("E")
							&& (st.startsWith(">RS") || st.startsWith(">FO") || st.startsWith(">MRS"))) {
						String reasons = st.split(">", 2)[1];
						String arr1[] = reasons.split(";", 2);
						al.add(arr1[0] + "_" + arr1[1]);
						st = brStart.readLine();
					}
				}

				while (st != null && !st.equals("E") && !st.startsWith(">STA")) {
					st = brStart.readLine();
				}

				boolean isSTAExistsAfterSTP = false;
				if (st != null && st.startsWith(">STA") && !st.equals("E")) {
					String[] arr22 = st.split(";");
					staStp.setStartTimestamp(arr22[1]);
					responsestart = ow.writeValueAsString(staStp);
					isSTAExistsAfterSTP = true;
				}

				staStp.setReasons(al);

				if (isSTAExistsAfterSTP) {
					eventStartStop.add(responsestart);
				} else if (isSTAExistsAfterSTP == false) {
					staStp.setStartTimestamp("null");
					responsestart = ow.writeValueAsString(staStp);
					eventStartStop.add(responsestart);
					break;
				}
			}

			if (flag == 0) {
				st = brStart.readLine();
			}
		}

		return eventStartStop;
	}

	/**
	 * Check the given date in the range
	 * 
	 * @param min
	 * @param max
	 * @param date
	 * @return
	 */
	public static boolean isDateInBetweenIncludingEndPoints(final Date min, final Date max, final Date date) {
		return date.after(min) && date.before(max);
	}
}
