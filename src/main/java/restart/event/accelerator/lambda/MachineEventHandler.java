package restart.event.accelerator.lambda;

import java.util.Timer;

import com.amazonaws.services.lambda.runtime.Context;

public class MachineEventHandler {

	static {

		try {
			Timer timer = new Timer();
			// Repeat publishing a message every 30 minutes
			timer.scheduleAtFixedRate(new MachineEventHandlerTask(), 100, 1800000);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Handler Method can be invoked from AWS Lambda
	 * 
	 * @param input
	 * @param context
	 * @return
	 */
	public String handleRequest(Object input, Context context) {
		return "Hello from java";
	}
}
