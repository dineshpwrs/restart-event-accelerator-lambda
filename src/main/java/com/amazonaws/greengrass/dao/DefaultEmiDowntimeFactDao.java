package com.amazonaws.greengrass.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.amazonaws.greengrass.db.Database;

public enum DefaultEmiDowntimeFactDao implements EmiDowntimeFactDao {
	instance;

	@Override
	public String getLatestTrpTime() {
		String str = null;
		String query = "select trp_time from emi_downtime_fact_table order by downtime_id desc limit 1";
		try (Connection conn = Database.connection(); PreparedStatement ps = conn.prepareStatement(query)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				str = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return str;
	}
}
