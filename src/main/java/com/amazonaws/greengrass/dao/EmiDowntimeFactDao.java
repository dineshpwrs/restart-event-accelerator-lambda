package com.amazonaws.greengrass.dao;

public interface EmiDowntimeFactDao {
	
	String getLatestTrpTime();

}
